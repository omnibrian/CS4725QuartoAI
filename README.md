# QuartoAI
QuartoAgent AI Project built for modified version of quarto provided in CS4725 course.

The game runs as a QuartoServer with two separate connected QuartoAgents.

## Compiling

To compile the quarto server and agents:

```
cd Quarto/
javac *.java
```

## Usage

To run the quarto server:

```
java QuartoServer
```

Start server with game state:

```
java QuartoServer state.quarto
```

## Connecting Agents to Server

To run an agent and have it connect to a server:

```
java QuartoAgent localhost
```

To start game with a specific board position:

```
java QuartoAgent localhost state.quarto
```
