public class QuartoAgent {

  static final int NUMBER_OF_ROWS = 5;
  static final int NUMBER_OF_COLUMNS = 5;
  private static final int NUMBER_OF_PIECES = 32;

  private static final String SELECT_PIECE_HEADER = "Q1:";
  private static final String SELECT_MOVE_HEADER = "Q2:";

  static final String INFO_MESSAGE_HEADER = "INFO:";

  private static final String ERROR_PIECE_HEADER = "ERR_PIECE:";
  private static final String ERROR_MOVE_HEADER = "ERR_MOVE:";

  private static final String ACKNOWLEDGMENT_PIECE_HEADER = "ACK_PIECE:";
  private static final String ACKNOWLEDGMENT_MOVE_HEADER = "ACK_MOVE:";

  static final String PIECE_MESSAGE_HEADER = "PIECE:";
  private static final String MOVE_MESSAGE_HEADER = "MOVE:";
  private static final String INFORM_PLAYER_NUMBER_HEADER = "PLAYER:";

  private static final String GAME_OVER_HEADER = "GAME_OVER:";
  private static final String TURN_TIME_LIMIT_HEADER = "TURN_TIME_LIMIT:";


  // Time limit is in milliseconds
  int timeLimitForResponse;
  static final int COMMUNICATION_DELAY = 500;
  private long startTime = System.currentTimeMillis();

  private static int playerNumber;
  private final GameClient gameClient;
  QuartoBoard quartoBoard;


  /**
   * Chooses a quarto piece for the other player to place.
   * Bit definitions from MSB to LSB:
   *  1. 0: short, 1: tall
   *  2. 0: hollow, 1: solid
   *  3. 0: black, 1: white
   *  4. 0: metal, 1: wood
   *  5. 0: square, 1: round
   *
   * @return String of '0's and '1's defining piece chosen.
   */
  protected String pieceSelectionAlgorithm() {
    // some useful lines:
    // String BinaryString = (Integer.toBinaryString(pieceId));
    // QuartoBoard copyBoard = new QuartoBoard(this.quartoBoard);

    // do work, then return e.g.
    return "10001";
  }

  /**
   * Chooses a move based on the quarto piece chosen.
   *
   * @param pieceId String of binary digits describing the piece chosen.
   * @return String with coordinates of where to place the piece formatted as "row,column".
   */
  protected String moveSelectionAlgorithm(int pieceId) {
    // QuartoBoard copyBoard = new QuartoBoard(this.quartoBoard);

    // do work, then return e.g.
    return "1,1";
  }


  /**
   * Main Method for QuartoAgent.
   * Starts game client to connect to server at IP specified in input args.
   *
   * @param args Array of input arguments as strings.
   */
  public static void main(String[] args) {
    // Start the server
    GameClient gameClient = new GameClient();

    String ip = null;
    String stateFileName = null;

    // IP must be specified
    if (args.length > 0) {
      ip = args[0];
    } else {
      System.out.println("No IP Specified");
      System.exit(0);
    }
    if (args.length > 1) {
      stateFileName = args[1];
    }

    gameClient.connectToServer(ip, 4321);
    QuartoAgent quartoAgent = new QuartoAgent(gameClient, stateFileName);
    quartoAgent.play();

    gameClient.closeConnection();

  }

  /**
   * QuartoAgent Constructor.
   * Sets the gameClient from input and creates quartoBoard for the QuartoAgent.
   *
   * @param gameClient GameClient connected to GameServer that the QuartoAgent will connect to.
   */
  public QuartoAgent(GameClient gameClient, String stateFileName) {
    this.gameClient = gameClient;
    this.quartoBoard = new QuartoBoard(NUMBER_OF_ROWS, NUMBER_OF_COLUMNS, NUMBER_OF_PIECES,
      stateFileName);
  }

  // Main game loop
  void play() {
    setPlayerNumber();
    setTurnTimeLimit();

    // Player 2 gets first move
    if (playerNumber == 2) {
      choosePieceTurn();
    }

    boolean gameOn = true;

    while (gameOn) {
      // Print board
      this.quartoBoard.printBoardState();
      // Turn order swaps
      chooseMoveTurn();

      this.quartoBoard.printBoardState();

      choosePieceTurn();
    }

  }

  // First response a client receives from server should be the player's number
  private void setPlayerNumber() {
    String message = this.gameClient.readFromServer(1000000);
    String[] splittedMessage = message.split("\\s+");
    System.out.println(splittedMessage[0] + " " + splittedMessage[1]);
    if (isExpectedMessage(splittedMessage, INFORM_PLAYER_NUMBER_HEADER)) {
      playerNumber = Integer.parseInt(splittedMessage[1], 10);
    } else {
      System.out.println("Did not Receive Player Number");
      System.exit(0);
    }

  }

  // Client should receive time limit from server
  private void setTurnTimeLimit() {
    String message = this.gameClient.readFromServer(1000000);
    String[] splittedMessage = message.split("\\s+");
    System.out.println(splittedMessage[0] + " " + splittedMessage[1]);
    if (isExpectedMessage(splittedMessage, TURN_TIME_LIMIT_HEADER)) {
      timeLimitForResponse = Integer.parseInt(splittedMessage[1], 10);
    } else {
      System.out.println("Did not Receive TURN_TIME_LIMIT_HEADER");
      System.exit(0);
    }

  }

  // Control flow for player choosing a piece
  private void choosePieceTurn() {
    String messageFromServer = this.gameClient.readFromServer(1000000);
    String[] splittedMessage = messageFromServer.split("\\s+");

    // Close program if message is not the expected message
    isExpectedMessage(splittedMessage, SELECT_PIECE_HEADER, true);

    // Determine piece
    String pieceMessage = pieceSelectionAlgorithm();

    this.gameClient.writeToServer(pieceMessage);

    messageFromServer = this.gameClient.readFromServer(1000000);
    String[] splittedResponse = messageFromServer.split("\\s+");
    if (!isExpectedMessage(splittedResponse, ACKNOWLEDGMENT_PIECE_HEADER)
        && !isExpectedMessage(splittedResponse, ERROR_PIECE_HEADER)) {
      turnError(messageFromServer);
    }

    messageFromServer = this.gameClient.readFromServer(1000000);
    String[] splittedMoveResponse = messageFromServer.split("\\s+");

    isExpectedMessage(splittedMoveResponse, MOVE_MESSAGE_HEADER, true);

    String[] moveString = splittedMoveResponse[1].split(",");
    int[] move = new int[2];
    move[0] = Integer.parseInt(moveString[0]);
    move[1] = Integer.parseInt(moveString[1]);

    int pieceId = Integer.parseInt(splittedResponse[1], 2);
    this.quartoBoard.insertPieceOnBoard(move[0], move[1], pieceId);
  }

  private void chooseMoveTurn() {
    String messageFromServer = this.gameClient.readFromServer(1000000);
    String[] splittedMessage = messageFromServer.split("\\s+");

    // Close program if message is not the expected message
    isExpectedMessage(splittedMessage, SELECT_MOVE_HEADER, true);
    int pieceId = Integer.parseInt(splittedMessage[1], 2);

    // Determine piece
    String moveMessage = moveSelectionAlgorithm(pieceId);

    this.gameClient.writeToServer(moveMessage);

    messageFromServer = this.gameClient.readFromServer(1000000);
    String[] splittedMoveResponse = messageFromServer.split("\\s+");
    if (!isExpectedMessage(splittedMoveResponse, ACKNOWLEDGMENT_MOVE_HEADER)
        && !isExpectedMessage(splittedMoveResponse, ERROR_MOVE_HEADER)) {
      turnError(messageFromServer);
    }

    String[] moveString = splittedMoveResponse[1].split(",");
    int[] move = new int[2];
    move[0] = Integer.parseInt(moveString[0]);
    move[1] = Integer.parseInt(moveString[1]);

    this.quartoBoard.insertPieceOnBoard(move[0], move[1], pieceId);
  }


  /*
   ********************* UTILITY FUNCTIONS ************************
   */
  private static void turnError(String message) {
    System.out.println("Turn order out of sync, last message received was: " + message);
    System.exit(0);
  }

  private static boolean isExpectedMessage(String[] splittedMessage, String header) {
    return isExpectedMessage(splittedMessage, header, false);
  }

  private static boolean isExpectedMessage(String[] splittedMessage, String header,
                                           boolean fatal) {
    if (splittedMessage.length > 0 && splittedMessage[0].equals(header)) {
      return true;
    } else if (splittedMessage.length > 0 && splittedMessage[0].equals(GAME_OVER_HEADER)) {
      gameOver(splittedMessage);
    } else if (fatal) {
      turnError(splittedMessage[0] + " " + splittedMessage[1]);
    }

    return false;
  }

  private static void gameOver(String[] splittedMessage) {
    StringBuilder builder = new StringBuilder();
    for (String i : splittedMessage) {
      builder.append(i).append(" ");
    }

    System.out.println(builder.toString());
    System.exit(0);
  }

  // Records the current time in milliseconds from when this function is called
  void startTimer() {
    startTime = System.currentTimeMillis();
  }

  // Gets the time difference between now and when startTimer() was last called
  long getMillisecondsFromTimer() {
    return System.currentTimeMillis() - startTime;
  }

  /*
   * Does a quick check (1 millisecond) for messages from the server.
   * This function will return the oldest missed message if there is one (and null if there is not).
   * If this function does return a message from the server, you may want to call this function
   * again to see if there are additional messages
   */
  protected String checkForMissedServerMessages() {
    return this.gameClient.readFromServer(1);
  }
}
