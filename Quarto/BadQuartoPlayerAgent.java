import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BadQuartoPlayerAgent extends QuartoAgent {
  private static final int GAME_SERVER_PORT = 4321;

  private static final Logger LOGGER = Logger.getLogger(QuartoPlayerAgent.class.getName());

  private BadQuartoPlayerAgent(GameClient gameClient, String stateFileName) {
    super(gameClient, stateFileName);
  }

  /**
   * Main Method for QuartoPlayerAgent.
   * Starts game client to connect to server at IP specified in input args and connects a new
   * QuartoPlayerAgent to it.
   *
   * @param args Array of input arguments as strings.
   */
  public static void main(String[] args) {
    // Setup logging
    Level logLevel = Level.FINE;

    Handler consoleHandler = new ConsoleHandler();
    consoleHandler.setLevel(logLevel);
    LOGGER.addHandler(consoleHandler);
    LOGGER.setLevel(logLevel);
    LOGGER.setUseParentHandlers(false);

    // Start the server
    GameClient gameClient = new GameClient();

    // Try connecting to server IP at args[0]
    try {
      gameClient.connectToServer(args[0], GAME_SERVER_PORT);
    } catch (IndexOutOfBoundsException ex) {
      String localIp = "localhost";

      LOGGER.warning(String.format(
          "No first argument specifying game server IP, attempting to connect on %s:%d.", localIp,
          GAME_SERVER_PORT
      ));

      gameClient.connectToServer(localIp, GAME_SERVER_PORT);
    }

    // Initialize agent with state if given
    String stateFileName = (args.length > 1) ? args[1] : null;
    BadQuartoPlayerAgent quartoAgent = new BadQuartoPlayerAgent(gameClient, stateFileName);

    try {
      quartoAgent.play();
    } catch (NullPointerException ex) {
      LOGGER.severe(String.format(
          "Caught NullPointerException while starting the quarto agent, this can be a symptom of "
          + "the quarto server at %s not running.", args[0]
      ));

      throw ex;
    }

    gameClient.closeConnection();
  }

  /**
   * Chooses a quarto piece for the other player to place.
   * Binary bit definitions from MSB to LSB for piece description:
   *  1. 0: short, 1: tall
   *  2. 0: hollow, 1: solid
   *  3. 0: black, 1: white
   *  4. 0: metal, 1: wood
   *  5. 0: square, 1: round
   *
   * @return String of '0's and '1's defining piece chosen.
   */
  @Override
  protected String pieceSelectionAlgorithm() {
    this.startTimer();
    boolean skip;

    for (int i = 0; i < this.quartoBoard.getNumberOfPieces(); i++) {
      skip = false;

      if (!this.quartoBoard.isPieceOnBoard(i)) {
        LOGGER.finest(String.format("Testing if %s can win", convertPieceToString(i)));

        for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
          for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
            if (!this.quartoBoard.isSpaceTaken(row, col) && goalTest(row, col, i)) {
              LOGGER.fine(String.format(
                  "Skipping possible winning piece: %s at row: %d, col: %d",
                  convertPieceToString(i), row, col
              ));

              skip = true;
              break;
            }
          }

          // Piece can win, exit loops to go to next piece
          if (skip) {
            break;
          }
        }

        // Piece doesn't win in any row / column combination
        if (!skip) {
          return convertPieceToString(i);
        }
      }
      /*
      if (this.getMillisecondsFromTimer() > (this.timeLimitForResponse - COMMUNICATION_DELAY)) {
        // Handle being over time limit
        // TODO: Account for communication delay
      }
      */

      // String message = null;

      // for every other i, check if there is a missed message
      /*
      if (i % 2 == 0 && ((message = this.checkForMissedServerMessages()) != null)) {
        // The oldest missed message is stored in the variable message.
        // Can see if any more missed messages are in the socket by running
        // this.checkForMissedServerMessages() again.
      }
      */
    }

    int pieceId = this.quartoBoard.chooseRandomPieceNotPlayed(100);
    return convertPieceToString(pieceId);
  }

  /**
   * Chooses a move based on the quarto piece chosen.
   *
   * @param pieceId String of binary digits describing the piece chosen.
   * @return String with coordinates of where to place the piece formatted as "row,column".
   */
  @Override
  protected String moveSelectionAlgorithm(int pieceId) {
    // If there is a winning move, take it
    for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
      for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
        if (!this.quartoBoard.isSpaceTaken(row, col) && goalTest(row, col, pieceId)) {
          return row + "," + col;
        }
      }
    }

    // If no winning move is found, return a random (unoccupied) square
    QuartoBoard copyBoard = new QuartoBoard(this.quartoBoard);
    int[] move = copyBoard.chooseRandomPositionNotPlayed(100);

    return move[0] + "," + move[1];
  }

  /**
   * Check if the given row, column, and piece satisfies the goal (winning the game).
   *
   * @param row Row that the quarto piece is to be inserted on.
   * @param col Column that the quarto piece is to be inserted on.
   * @param pieceId Quarto piece id to be played.
   * @return True if putting the piece at the specified location will win the game, false otherwise.
   */
  private boolean goalTest(int row, int col, int pieceId) {
    QuartoBoard copyBoard = new QuartoBoard(this.quartoBoard);
    copyBoard.insertPieceOnBoard(row, col, pieceId);

    return (copyBoard.checkRow(row) || copyBoard.checkColumn(col) || copyBoard.checkDiagonals());
  }

  /**
   * Check if the Quarto Board satisfies the goal (game is won).
   *
   * @param quartoBoard Initialized quarto board.
   * @return True if there is a row, column or diagonal that has matching pieces, false otherwise.
   */
  private boolean goalTest(QuartoBoard quartoBoard) {
    if (quartoBoard.checkDiagonals()) {
      return true;
    }

    boolean goalFound;

    for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
      for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
        goalFound = (quartoBoard.checkRow(row) || quartoBoard.checkColumn(col));

        if (goalFound) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * Builds a list of possible actions from empty spots on quarto board to place the piece.
   *
   * @param quartoBoard Initialized quarto board to place piece in.
   * @param pieceId Piece to place in quarto board.
   * @return List of actions for putting piece in an empty spot on quarto board.
   */
  private List<Action> getPossibleActions(QuartoBoard quartoBoard, int pieceId) {
    List<Action> actions = new ArrayList<>();

    for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
      for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
        if (!quartoBoard.isSpaceTaken(row, col)) {
          actions.add(new Action(row, col, pieceId));
        }
      }
    }

    return actions;
  }

  /**
   * Convert integer representing a quarto piece into a string of 5 binary characters.
   *
   * @param pieceId Quarto piece id.
   * @return String of 5 binary characters describing quarto piece passed in.
   */
  private String convertPieceToString(int pieceId) {
    return String.format("%5s", Integer.toBinaryString(pieceId)).replace(' ', '0');
  }



  private class Action {
    private int pieceId;
    private int row;
    private int col;

    Action(int row, int col, int pieceId) {
      this.pieceId = pieceId;
      this.row = row;
      this.col = col;
    }

    public int getPieceId() {
      return pieceId;
    }

    public int getRow() {
      return row;
    }

    public int getCol() {
      return col;
    }

    public QuartoBoard getNewState(QuartoBoard board) {
      board.insertPieceOnBoard(row, col, pieceId);
      return board;
    }
  }
}
