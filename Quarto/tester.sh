#!/usr/bin/env bash
COUNTER=0
COUNT1=0
COUNT2=0
COUNTDRAW=0

while [ $COUNTER -lt 10 ]; do
  output=$(java QuartoSemiRandomAgent localhost &>/dev/null & sleep 1 && java QuartoPlayerAgent localhost 2>/dev/null | tail -1)
  if echo $output | grep 'player 1 wins'; then
    let COUNT1=$COUNT1+1
  elif echo $output | grep 'player 2 wins'; then
    let COUNT2=$COUNT2+1
  else
    echo $output
    let COUNTDRAW=$COUNTDRAW+1
  fi
  let COUNTER=$COUNTER+1
done

echo "SemiRandom won $COUNT1 times"
echo "Player won $COUNT2 times"
echo "Draw $COUNTDRAW times"
