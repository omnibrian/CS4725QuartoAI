import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuartoPlayerAgent extends QuartoAgent {
  private static final int GAME_SERVER_PORT = 4321;
  private static final int TREE_SIM_TIME = 1000;
  private static final int MAX_BREADTH = 5;
  private static final int MAX_DEPTH = 5;

  private static final Logger LOGGER = Logger.getLogger(QuartoPlayerAgent.class.getName());

  private QuartoPlayerAgent(GameClient gameClient, String stateFileName) {
    super(gameClient, stateFileName);
  }

  /**
   * Main Method for QuartoPlayerAgent.
   * Starts game client to connect to server at IP specified in input args and connects a new
   * QuartoPlayerAgent to it.
   *
   * @param args Array of input arguments as strings.
   */
  public static void main(String[] args) {
    // Setup logging
    Level logLevel = Level.FINE;

    Handler consoleHandler = new ConsoleHandler();
    consoleHandler.setLevel(logLevel);
    LOGGER.addHandler(consoleHandler);
    LOGGER.setLevel(logLevel);
    LOGGER.setUseParentHandlers(false);

    // Start the server
    GameClient gameClient = new GameClient();

    // Try connecting to server IP at args[0]
    try {
      gameClient.connectToServer(args[0], GAME_SERVER_PORT);
    } catch (IndexOutOfBoundsException ex) {
      String localIp = "localhost";

      LOGGER.warning(String.format(
          "No first argument specifying game server IP, attempting to connect on %s:%d.", localIp,
          GAME_SERVER_PORT
      ));

      gameClient.connectToServer(localIp, GAME_SERVER_PORT);
    }

    // Initialize agent with state if given
    String stateFileName = (args.length > 1) ? args[1] : null;
    QuartoPlayerAgent quartoAgent = new QuartoPlayerAgent(gameClient, stateFileName);

    // Start agent playing game
    quartoAgent.play();

    gameClient.closeConnection();
  }

  /**
   * Chooses a quarto piece for the other player to place.
   * Binary bit definitions from MSB to LSB for piece description:
   *  1. 0: short, 1: tall
   *  2. 0: hollow, 1: solid
   *  3. 0: black, 1: white
   *  4. 0: metal, 1: wood
   *  5. 0: square, 1: round
   *
   * @return String of '0's and '1's defining piece chosen.
   */
  @Override
  protected String pieceSelectionAlgorithm() {
    this.startTimer();
    int checkStartTime;
    int checkEndTime;
    boolean skip = false;

    List<Integer> losingPieces = new ArrayList<>();

    // Find pieces that won't allow opponent to win
    for (int i = 0; i < this.quartoBoard.getNumberOfPieces(); i++) {
      skip = false;

      if (!this.quartoBoard.isPieceOnBoard(i)) {
        for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
          for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
            if (!this.quartoBoard.isSpaceTaken(row, col) && goalTest(row, col, i)) {
              LOGGER.finest(String.format(
                  "Skipping possible winning piece: %s at row: %d, col: %d",
                  convertPieceToString(i), row, col
              ));

              skip = true;
              break;
            }
          }

          // Piece can win, exit loops to go to next piece
          if (skip) {
            break;
          }
        }

        // Piece doesn't win in any row / column combination
        if (!skip) {
          // return convertPieceToString(i);
          losingPieces.add(i);
        }
      }
    }

    // If no losing pieces found, all pieces not played are winning pieces
    if (losingPieces.size() == 0) {
      LOGGER.fine("Could only find winning pieces");
      return convertPieceToString(this.quartoBoard.chooseRandomPieceNotPlayed(100));
    }

    LOGGER.fine("Found " + losingPieces.size() + " losing pieces");

    // Shuffle to avoid looking at pieces in order of search
    Collections.shuffle(losingPieces);

    List<Integer> possibleWins = new ArrayList<>();
    int numWins;
    int maxWins = Integer.MIN_VALUE;
    int maxWinsIndex = 0;

    int treeSimTime = TREE_SIM_TIME;
    int timeLimit = this.timeLimitForResponse - COMMUNICATION_DELAY;

    for (int i = 0; i < losingPieces.size(); i++) {
      // Stop simulating if getting too close to time limit
      if (this.getMillisecondsFromTimer() > (timeLimit - treeSimTime)) {
        break;
      }

      checkStartTime = (int)this.getMillisecondsFromTimer();

      numWins = continueGameFromPiece(losingPieces.get(i), this.quartoBoard, false, 1);

      LOGGER.fine("Got " + numWins + " wins for piece: "
          + convertPieceToString(losingPieces.get(i)));

      if (numWins > maxWins) {
        maxWins = numWins;
        maxWinsIndex = i;
      }

      possibleWins.add(numWins);

      checkEndTime = (int)this.getMillisecondsFromTimer();

      // Fix sim time based on current run
      if ((checkEndTime - checkStartTime) > treeSimTime) {
        treeSimTime = checkEndTime - checkStartTime + 100;
      }
    }

    return convertPieceToString(losingPieces.get(maxWinsIndex));
  }

  /**
   * Continue simulating game tree from a piece chosen and a board state.
   *
   * @param pieceId Piece chosen to be played
   * @param boardState Current state of the quarto game board
   * @param turnId Identifier for which players turn is being simulated
   * @param depth Current stack depth to track how far to expand game tree
   * @return Utility for how many different game tree options can be wins
   */
  protected int continueGameFromPiece(int pieceId, QuartoBoard boardState, boolean turnId,
      int depth) {
    List<int[]> freeSpaces = new ArrayList<>();

    // Return if there is a winning move to take
    for (int row = 0; row < boardState.getNumberOfRows(); row++) {
      for (int col = 0; col < boardState.getNumberOfColumns(); col++) {
        if (!boardState.isSpaceTaken(row, col)) {
          if (goalTest(row, col, pieceId, boardState)) {
            LOGGER.fine("Win found at depth " + depth);

            if (turnId) {
              return 1 * (MAX_DEPTH - depth + 1);
            } else {
              return -1 * (MAX_DEPTH - depth + 1);
            }
          }

          int[] freeMove = {row, col};
          freeSpaces.add(freeMove);
        }
      }
    }

    // Assume a tie if max depth is reached without a win
    if (depth >= MAX_DEPTH) {
      return 0;
    }

    Collections.shuffle(freeSpaces);

    int possibleWins = 0;

    for (int i = 0; i < freeSpaces.size() && i < MAX_BREADTH; i++) {
      QuartoBoard newBoardState = new QuartoBoard(boardState);
      newBoardState.insertPieceOnBoard(freeSpaces.get(i)[0], freeSpaces.get(i)[1], pieceId);

      possibleWins += continueGameFromMove(newBoardState, turnId, depth + 1);
    }

    return possibleWins;
  }

  /**
   * Chooses a move based on the quarto piece chosen.
   *
   * @param pieceId String of binary digits describing the piece chosen.
   * @return String with coordinates of where to place the piece formatted as "row,column".
   */
  protected String moveSelectionAlgorithm(int pieceId) {
    this.startTimer();
    int checkStartTime;
    int checkEndTime;

    List<int[]> freeSpaces = new ArrayList<>();

    // If there is a winning move, take it
    for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
      for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
        if (!this.quartoBoard.isSpaceTaken(row, col)) {
          if (goalTest(row, col, pieceId)) {
            return row + "," + col;
          }
          int[] freeMove = {row, col};
          freeSpaces.add(freeMove);
        }
        if (!this.quartoBoard.isSpaceTaken(row, col) && goalTest(row, col, pieceId)) {
          return row + "," + col;
        }
      }
    }

    LOGGER.fine("Found " + freeSpaces.size() + " free spaces");

    // Shuffle contents of freeSpaces
    Collections.shuffle(freeSpaces);

    List<Integer> possibleWins = new ArrayList<>();

    int numWins;
    int maxWins = Integer.MIN_VALUE;
    int maxWinsIndex = 0;

    int treeSimTime = TREE_SIM_TIME;
    int timeLimit = this.timeLimitForResponse - COMMUNICATION_DELAY;

    for (int i = 0; i < freeSpaces.size(); i++) {
      if (this.getMillisecondsFromTimer() > (timeLimit - treeSimTime)) {
        break;
      }

      checkStartTime = (int)this.getMillisecondsFromTimer();

      QuartoBoard newBoardState = new QuartoBoard(this.quartoBoard);
      newBoardState.insertPieceOnBoard(freeSpaces.get(i)[0], freeSpaces.get(i)[1], pieceId);

      numWins = continueGameFromMove(newBoardState, true, 1);

      LOGGER.fine("Got " + numWins + " wins for move: "
          + freeSpaces.get(i)[0] + "," + freeSpaces.get(i)[1]);

      if (numWins > maxWins) {
        maxWins = numWins;
        maxWinsIndex = i;
      }

      possibleWins.add(numWins);

      checkEndTime = (int)this.getMillisecondsFromTimer();

      // Update time limit based on current run
      if ((checkEndTime - checkStartTime) > treeSimTime) {
        treeSimTime = checkEndTime - checkStartTime + 100;
      }
    }

    int[] move = freeSpaces.get(maxWinsIndex);
    return move[0] + "," + move[1];
  }

  /**
   * Continue simulating game tree from a piece played in the board state.
   *
   * @param boardState Current state of the quarto game board
   * @param turnId Identifier for which players turn is being simulated
   * @param depth Current stack depth to track how far to expand game tree
   * @return Utility for how many different game tree options can be wins
   */
  protected int continueGameFromMove(QuartoBoard boardState, boolean turnId, int depth) {
    // Return if game is won
    if (goalTest(boardState)) {
      LOGGER.fine("Win found from move at depth " + depth);
      if (turnId) {
        return 1 * (MAX_DEPTH - depth + 1);
      } else {
        return -1 * (MAX_DEPTH - depth + 1);
      }
    }

    // Assume a tie if at max depth
    if (depth >= MAX_DEPTH) {
      return 0;
    }

    if (boardState.checkIfBoardIsFull()) {
      return 0;
    }

    List<Integer> losingPieces = new ArrayList<>();
    boolean skip;

    // Find pieces that won't allow opponent to win
    for (int i = 0; i < this.quartoBoard.getNumberOfPieces(); i++) {
      skip = false;

      if (!boardState.isPieceOnBoard(i)) {
        for (int row = 0; row < this.quartoBoard.getNumberOfRows(); row++) {
          for (int col = 0; col < this.quartoBoard.getNumberOfColumns(); col++) {
            if (!boardState.isSpaceTaken(row, col) && goalTest(row, col, i, boardState)) {
              LOGGER.finest(String.format(
                  "Skipping possible winning piece: %s at row: %d, col: %d",
                  convertPieceToString(i), row, col
              ));

              skip = true;
              break;
            }
          }

          // Piece can win, exit loops to go to next piece
          if (skip) {
            break;
          }
        }

        // Piece doesn't win in any row / column combination
        if (!skip) {
          // return convertPieceToString(i);
          losingPieces.add(i);
        }
      }
    }

    if (losingPieces.size() == 0) {
      if (turnId) {
        return -1 * (MAX_DEPTH - depth);
      } else {
        return 1 * (MAX_DEPTH - depth);
      }
    }

    Collections.shuffle(losingPieces);

    int possibleWins = 0;

    for (int i = 0; i < losingPieces.size() && i < MAX_BREADTH; i++) {
      possibleWins += continueGameFromPiece(losingPieces.get(i), boardState, !turnId, depth + 1);
    }

    return possibleWins;
  }

  /* ==================== Utility Functions ==================== */
  /**
   * Check if the given row, column, and piece satisfies the goal (winning the game).
   *
   * @param row Row that the quarto piece is to be inserted on.
   * @param col Column that the quarto piece is to be inserted on.
   * @param pieceId Quarto piece id to be played.
   * @return True if putting the piece at the specified location will win the game, false otherwise.
   */
  private boolean goalTest(int row, int col, int pieceId) {
    QuartoBoard copyBoard = new QuartoBoard(this.quartoBoard);
    copyBoard.insertPieceOnBoard(row, col, pieceId);

    return (copyBoard.checkRow(row) || copyBoard.checkColumn(col) || copyBoard.checkDiagonals());
  }

  /**
   * Check if the given row, column, and piece satisfies the goal (winning the game).
   *
   * @param row Row that the quarto piece is to be inserted on.
   * @param col Column that the quarto piece is to be inserted on.
   * @param pieceId Quarto piece id to be played.
   * @param boardState State of the board where piece is being played at location.
   * @return True if putting the piece at the specified location will win the game, false otherwise.
   */
  private boolean goalTest(int row, int col, int pieceId, QuartoBoard boardState) {
    QuartoBoard copyBoard = new QuartoBoard(boardState);
    copyBoard.insertPieceOnBoard(row, col, pieceId);

    return (copyBoard.checkRow(row) || copyBoard.checkColumn(col) || copyBoard.checkDiagonals());
  }

  /**
   * Check if the Quarto Board satisfies the goal (game is won).
   *
   * @param quartoBoard Initialized quarto board.
   * @return True if there is a row, column or diagonal that has matching pieces, false otherwise.
   */
  private boolean goalTest(QuartoBoard quartoBoard) {
    if (quartoBoard.checkDiagonals()) {
      return true;
    }

    for (int row = 0; row < quartoBoard.getNumberOfRows(); row++) {
      if (quartoBoard.checkRow(row)) {
        return true;
      }
    }

    for (int col = 0; col < quartoBoard.getNumberOfColumns(); col++) {
      if (quartoBoard.checkColumn(col)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Convert integer representing a quarto piece into a string of 5 binary characters.
   *
   * @param pieceId Quarto piece id.
   * @return String of 5 binary characters describing quarto piece passed in.
   */
  private String convertPieceToString(int pieceId) {
    return String.format("%5s", Integer.toBinaryString(pieceId)).replace(' ', '0');
  }
}
