\documentclass[titlepage, letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{listings}

\title{
    CS4725: Intro to Artificial Intelligence \\
    Quarto Programming Project Report \\
}
\author{Brian LeBlanc (3479379)}
\date{\today}

\begin{document}
\maketitle

\section{Technique Used}

This project is based off the QuartoSemiRandomAgent that was completed in
assignment one. The implementation of move selection follows the technique of
searching for a winning move and returns that move if found. The implementation
of piece selection builds a list of all pieces not on the board that will not
allow the opposing player to win and chooses a piece from that list. For piece
and move selection where no winning move is found, a Monte Carlo and minimax
approach is used for finding a suitable choice.

The player does not take into account any previous moves because of the
possibility of the game being started from a specific board state and the
complexity of keeping track of previous moves.

Both selection algorithms use an alpha-beta type approach where there are two
recursive functions that call each other. Instead of a min function and a max
function, one function chooses a piece and the other chooses a move. Both return
a sum of the calculated utility for each sub-tree which is based on the player
turn where the game ends. Since there is a time limit, the breadth and depth of
the game tree expansion are both limited to at most five. If a winning move is
found on a move choice, the assumption is that both players will choose that
move and the same strategy can be applied in reverse to piece selection where it
is assumed both players will avoid choosing a winning piece.

The utility is calculated based on whether the game is a win or a loss for the
current player. If a game finishes soon in the game tree a higher weight is used
for the utility returned so that a closer win is favored. The return value is
negative if the game would be ending in the opposing player winning.

To make the most of the available time limit, piece selection and move selection
will keep processing possible choices until close to the time limit or all
suitable choices have been evaluated and will return the choice with the highest
calculated utility.

\section{Results}

When playing the QuartoPlayerAgent developed by me for this project against the
QuartoSemiRandomAgent, the player developed by me won on average $95\%$ of the
games with the rest of the games ending in a draw. When facing players built by
other classmates, games ended mostly in draws with some wins. Up to $20\%$ of
games against specifically one classmate's player ended in a loss for the player
developed by me.

Since the implementation of the agent is far from perfect and is not using a
specific adversarial method presented in class, this is a better outcome than
what was expected. The results show that the player developed by me is operating
at a similar level to most of the other players developed by classmates.

The player worked so well due to there being enough simulation data for the
algorithms to choose a better play than simply the first random move or
non-winning piece. There is enough simulation depth for the player to be
thinking a few choices ahead of the current game state to decide which choice is
closest to optimal.

\section{Improvements}

While the results seen from playing the QuartoPlayerAgent against other players
are fairly positive, they show that there is room for improvement. When looking
at the logs from running the QuartoPlayerAgent, up until about half way through
a game, all game simulations find that each choice evaluates to 0. This is
because all of the simulations reach maximum depth before getting to a win or
loss. This leads to the QuartoPlayerAgent choosing a piece or move at random.
The player could be improved to avoid such unnecessary processing.

The first possible improvement for the player is to use a heuristic function to
evaluate any given board state. Unfortunately due to time constraints, there
wasn't enough time to develop an appropriate heuristic function. The algorithm
currently assumes a tied game for any simulation that goes beyond a depth of 5.
With a heuristic function to evaluate the board once the maximum depth is
reached, a more appropriate value could be returned for the board state reached
allowing for the algorithm to make a better decision on which action to take.
This would be especially helpful in early simulations so that pieces and moves
aren't simply chosen at random.

The second improvement that could be made is to have the algorithm try to use
real world quarto strategies. One such strategy is blocking a nearly winning
row, column, or diagonal with an opposite piece so that win cannot be made on
that row, column, or diagonal. Another quarto strategy that could be implemented
is to pick a specific attribute and keep choosing pieces with that attribute for
the other player to play until there is an almost full line with pieces having
that attribute. These are also improvements that would affect more the first
part of the game where the player is currently choosing pieces and moves at
random.

A third improvement would be to find the optimal breadth and depth limits.
Thinking more specifically about the breadth limitation, several parts of the
game tree go unsimulated. In order for the simulation to return within a
reasonable amount of time, the simulation must be limited in breadth and depth.
Once again due to not having the time to do trial and error to find the optimal
depth and branching these are left at what currently gives the best results
within the necessary time frame.

\newpage

\section{Functions Implemented}

\noindent \textbf{pieceSelectionAlgorithm()}: \\
$losingPieces\leftarrow$ pieces not in play and that cannot win \\
if $losingPieces$ is empty, return a random piece \\
for $losingPiece$ in $losingPieces$: \\
\indent $continueGameFromPiece(losingPiece,boardState,!myTurn,1)$ \\
return $losingPiece$ matching minimum $continueGameFromPiece$ value \\

\noindent \textbf{continueGameFromPiece(pieceId,boardState,turnId,depth)}: \\
if find winning move with $pieceId$: \\
\indent if $turnId=myTurn$, return \textbf{win} \\
\indent else, return \textbf{loss} \\
if reached max $depth$, return \textbf{tie} \\
$freeSpace\leftarrow$ any space open on the board \\
$possibleWins\leftarrow0$ \\
for $space$ in $freeSpace$ (up to $MAX\_BREADTH$): \\
\indent $newBoardState\leftarrow$ new boardState with $pieceId$ at $space$ \\
\indent add $continueGameFromMove(newBoardState,turnId,depth+1)$ to $possibleWins$ \\
return $possibleWins$ \\

\noindent \textbf{moveSelectionAlgorithm(pieceId)}: \\
Look for any winning moves, if found return that move \\
$freeSpaces\leftarrow$ any space open on the board \\
$possibleWins\leftarrow\emptyset$ \\
for $space$ in $freeSpaces$: \\
\indent $newBoardState\leftarrow$ new boardState with $pieceId$ placed at $space$ \\
\indent $continueGameFromMove(newBoardState,myTurn,1)$ \\
return $space$ matching highest $continueGameFromMove$ value \\

\noindent \textbf{continueGameFromMove(boardState,turnId,depth)}: \\
if $boardState$ is a winning state: \\
\indent if $turnId=myTurn$, return \textbf{win} \\
\indent else, return \textbf{lose} \\
if reached max $depth$, return \textbf{tie} \\
if $boardState$ is a full board, return \textbf{tie} \\
$losingPieces\leftarrow$ pieces not in play and that cannot win \\
if $losingPieces$ is empty: \\
\indent if $turnId=myTurn$, return \textbf{lose} \\
\indent else, return \textbf{win} \\
$possibleWins\leftarrow0$ \\
for $piece$ in $losingPieces$ (up to $MAX\_BREADTH$): \\
\indent add $continueGameFromPiece(losingPieces[i],boardState,!turnId)$ to $possibleWins$ \\
return $possibleWins$

\end{document}
